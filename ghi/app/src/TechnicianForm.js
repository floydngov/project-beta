import React, {useState, useEffect} from "react"

function TechnicianForm () {
    const [techName, setTechName] = useState('');  //intitialized varible techName sets state to blank
    const [techNumber, setTechNumber] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const getTechnician = async () => {
      const URL = "http://localhost:8080/api/technicians/";
      const response = await fetch(URL);
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians)
        //console.log(data.technicians)
      }
    }
    useEffect(() => {
      getTechnician();
    }, []);


    const handleTechChange =  (event) => {   // method for when change in form happens
        const value = event.target.value;    // gets access to key that is pressed
        setTechName(value);                 //updates the value 
    }
   
    const handleNumberChange = (event) => {
        const value = event.target.value;
        setTechNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();         //prevents from going somewhere 
        const data = {};        // empty data object 
        data.name = techName;           // assign state values to key names that the back end server is expecting."
        data.employee_number = techNumber;  //"data.name & data.employee_number are names in models "
        //console.log(data);

        const techUrl = 'http://localhost:8080/api/technicians/'; //sends to 
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(data)
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            console.log("ok")                                   //resets to blank
            const newTech = await response.json();    //set state to empty strings 
            setTechName("");                        //use those values for the form elements.
            setTechNumber("");
            
        }

    }
    
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Technician</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleTechChange} value={techName} placeholder="Tech Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNumberChange} value={techNumber} placeholder="Tech number" required type="text" name="techNumber" id="tech_number" className="form-control" />
                            <label htmlFor="tech_number">Tech number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm