import React, { useEffect, useState, } from "react";

function AppointmentList() {
    const [appointments, setAppt] = useState([]);
    
    const fetchData = async () => {
        const URL = "http://localhost:8080/api/appointments/";
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            setAppt(data);
        }
    }
    const deleteApt = async (id) => {
        const URL = (`http://localhost:8080/api/appointments/${id}/`)
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(URL, fetchConfig)
        if (response.ok) {
            fetchData();
            const data = await response.json()
            console.log(data)
        }
    }
    const finished = async (id) => {
        const URL = (`http://localhost:8080/api/appointments/${id}/`);
        
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ completed: true }),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(URL, fetchConfig);
        if (response.ok) {
            fetchData();
            const data = await response.json();
            
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    
    return(
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Reason</th>
            <th>Tech</th>
            <th>Vip</th>
            <th>Cancel</th>
            <th>Finished</th>
          </tr>
        </thead>
                            {/* asked if array existed  */}
        <tbody>                
            {appointments.appointments?.map((appt) => {
                return (
                    <tr key={appt.id}>
                        <td>{appt.vin}</td>
                        <td>{appt.customer_name}</td>
                        <td>{appt.date}</td>
                        <td>{appt.reason}</td>
                        <td>{appt.technician.name}</td>
                        <td>VIP</td>
                        <td>
                            <button type="button" className="btn btn-danger" onClick={() => deleteApt(appt.id)}>Cancel</button>
                        </td>
                        <td>
                            <button type="button" className="btn btn-success" onClick={() => finished(appt.id)}>Completed</button>
                        </td>
                    </tr>
                )
            })}
          
        </tbody>
      </table>
    );
  }
  
  export default AppointmentList;
