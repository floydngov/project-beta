import React, {useState} from 'react';

function SalesCustomerForm(props) {
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')
    const [submitted, setSubmitted] = useState(false)

    const handleChangeName = (event) => {
        const value = event.target.value
        setName(value);
    }

    const handleChangeAddress = (event) => {
        const value = event.target.value
        setAddress(value);
    }

    const handleChangePhoneNumber = (event) => {
        const value = event.target.value
        setPhoneNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const newSalesCustomer = {
            "name": name,
            "address": address,
            "phone_number": phone_number
        }

        const salesCustomerURL = 'http://localhost:8090/api/salescustomers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(newSalesCustomer),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(salesCustomerURL, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newSalesCustomer = await response.json();
            console.log(newSalesCustomer)
            setName("");
            setAddress("");
            setPhoneNumber("");
            setSubmitted(true);
            props.getCustomer();
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Sales Customer</h1>
                    <form onSubmit={handleSubmit} id="create-salesCustomer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeName} placeholder="Name" required
                            type="text" name ="name" id="name"
                            className="form-control" value={name}/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeAddress} placeholder="Address" required
                            type="text" name ="address" id="address"
                            className="form-control" value={address}/>
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangePhoneNumber} placeholder="Phone Number" required
                            type="number" name ="phone_number" id="phone_number"
                            className="form-control" value={phone_number}/>
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div><br/>

            </div>
        </div>


    );
}

export default SalesCustomerForm
