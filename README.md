** For correct formatting, please view as Display Source **

# CarCar
CarCar is a luxury car dealership inventory and customer management application that efficiently logs vehicle manufacturers, models, and automobiles for its inventory functionality. Additionally, this application provides management of automobile services, sales history, and logging of new sales.

### Inventory Functionality:
- View all current manufacturers, Add a manufacturer, view current vehicle models, add vehicle models, view available automobiles, and add automobiles.

### Sales Functionality:
- Creates/add new salesperson, create/add new customer, view list of all dealership sales, view list of sales by salesperson, and create a new sales record.


### Service Functionality:  
- Allows the use to create/add new technicians, create/add new appointments, views the list of all appointments, and lets you view the service history of all a a specific car


### CarCar Dev Team:
- Floyd Ngov - Sales
- Israel Navarrete - Service

### How to Run Application:

Installation requirements:
- Python 3 = https://www.python.org/downloads/
- Docker = https://www.docker.com/get-started/
- Insomnia = https://insomnia.rest/

## Setup:
1. Fork CarCar application from Gitlab link: https://gitlab.com/i.navarrete/project-beta.git
- commands: cd <your directory>
git clone https://gitlab.com/i.navarrete/project-beta.git

2. CD into the new repository:
- Command: cd project-beta

3. (Optional) Set up virtual environment:
- Commands: python -m venv .venv
- source venv/bin/activate

4. (If applicable) Deactivate virtual environment:
-Command: deactivate

5. Update pip:
- Command: python -m pip install –upgrade pip

6. Install all dependencies from requirements.txt:
- Command: python install -r requirements.txt

### Docker: CarCar containers run on port 8000 by default

1. Setup Docker volume for data:
- Command: docker volume create beta-data

2. Build Docker Image:
- Command: docker-compose build

3. Run Containers:
- Command: docker-compose up

## Diagram:
https://excalidraw.com/#json=SK7BDtydtZM88WXdcm8IL,9nz_fS9_RzuJTLp9ganjvA

## Inventory: localhost:8100 (Django Backend)

- inventory_rest: Inventory backend Python files
	- models.py: Inventory models (Manufacturer, VehicleModel, Automobile)
	- encoders.py: Inventory model encoders
	- urls.py: Url paths for inventory HTTP requests
	- views.py: View functions that convert HTTP requests to responses

## URL Paths/Function Names
- URL: api/manufacturers/ Function: api_manufacturers
- URL: api/manufacturers/int:pk/ Function: api_manufacturer
- URL: api/models/ Function: api_vehicle_models
- URL: api/models/int:pk/ Function: api_vehicle_model
- URL: api/automobiles/ Function: api_automobiles
- URL: api/automobiles/vin/ Function: api_automobile

## Functions:
api_manufacturers
- GET all Manufacturer data and POST (create) a new Manufacturer

api_manufacturer
- GET a specific Manufacturer data, DELETE a specific Manufacturer, and PUT (update) a specific Manufacturer data

api_vehicle_models
- GET all Vehicle Model data and POST (create) a new Vehicle Model

api_vehicle_model
- GET a specific Vehicle Model data, DELETE a specific Vehicle Model, and PUT(update) a specific Vehicle Model data

api_automobiles
- GET all Automobile data and POST (create) a new Automobile

api_automobile
- GET a specific Automobile data, DELETE a specific Automobile, and PUT(update) a specific Automobile data

## Example JSON inputs and returns:
- POST request to: api/automobiles/
- Request body:
```
{
  "color": "yellow",
  "year": 2022,
  "vin": "WDDNG7BB0AA245942",
  "model_id": 4
}
```
Returns (status code 200)
```
{
	"href": "/api/automobiles/WDDNG7BB0AA245942/",
	"id": 11,
	"color": "yellow",
	"year": 2022,
	"vin": "WDDNG7BB0AA245942",
	"model": {
		"href": "/api/models/4/",
		"id": 4,
		"name": "M3",
		"picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1rQlyrsXd6_humPdj2dwUBKfOg0mrOd7o9w&usqp=CAU",
		"manufacturer": {
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "BMW"
		}
	}
}
```
Get request to: api/automobiles/vin/
- Returns (status code 200)
```
{
	"href": "/api/automobiles/1FTHF25H1JNB79097/",
	"id": 7,
	"color": "black",
	"year": 2022,
	"vin": "1FTHF25H1JNB79097",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "R1S SUV",
		"picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTMqm-wTFH13HJtcJqUL0Ut9G6sw61n7dWXQ&usqp=CAU",
		"manufacturer": {
			"href": "/api/manufacturers/6/",
			"id": 6,
			"name": "Rivian"
		}
	}
}
```
# Sales: localhost:8090 (Django Backend)
- Poll: Contains poller.py
	- poller.py: Takes automobile data from inventory database and attaches  to AutomobileVO (value object)

- sales_rest: Sales backend Python files
	-  models.py: Sales models (AutomobileVO, SalesPerson, SalesCustomer, SalesRecord), Sales model encoders
	- urls.py: Url paths for Sales HTTP requests
	- views.py: View functions that convert HTTP requests to responses

## URL Paths/Function Names
- URL: api/salespersons/ Function: api_sales_persons
- URL: api/salespersons/int:pk/ Function: api_sales_person
- URL: api/salescustomers/ Function: api_sales_customers
- URL: api/salescustomers/int:pk/ Function: api_sales_customer
- URL: api/salesrecords/ Function: api_list_salesrecords
- URL: api/salesrecords/int:pk/ Function: api_sales_record_details

## Functions:
api_sales_persons
- GET all Sales Person data and POST (create) a new Sales Person

api_sales_person
- GET a specific Sales Person data, DELETE a specific Sales Person, and PUT (update)
a specific Sales Person data

api_sales_customers
- GET all Sales Customer data and POST (create) a new Sales Customer

api_sales_customer
- GET a specific Sales Customer data, DELETE a specific Sales Customer, and PUT(update) a specific Sales Customer data

api_lists_salesrecords
- GET all Sales Record data and POST (create) a new Sales Record

api_sales_record_details
- GET a specific Sales Record data and DELETE a specific Sales Record

## Example JSON inputs and returns:
- POST request to: api/salespersons/
Request Body:
```
{
  "id": 9,
  "name": "Bob",
  "employee_number": "9"
}
```
Returns (status code 200)
```
{
	"href": "/api/salespersons/9/",
	"id": 9,
	"name": "Bob",
	"employee_number": "9"
}
```
GET request to: api_sales_record_details
- Returns (status code 200)
```
{
	"sales_person": {
		"name": "Israel",
		"emp_no": 2
	},
	"automobile": "1NXBR12E21Z431720",
	"sales_customer": "James",
	"price": "70000",
	"id": 5
}
```
POST request to: api/salesrecords/
- Request Body:
```
{
"automobile": "WDDNG7BB0AA245942",
"sales_person": "Floyd",
"sales_customer": "Lynn",
"price": "90000"
}
```
Returns (status code 200)
```
{
	"sales_person": {
		"name": "Floyd",
		"emp_no": 1
	},
	"automobile": "WDDNG7BB0AA245942",
	"sales_customer": "Lynn",
	"price": "90000",
	"id": 8
}
```

# Services: localhost:8080 (django back end)

### Services_rest: Services Backend Python files
- Models.py: Services models ( AutomobileVO, Technician, Appointment)
- Urls.py: Contains the path for the http requests 
- Views.py: Contains the view functions the convert http request into responses
 
## URL Path/Function Names ,
- URL: api/technicians/  Function: api_list_technicians
- URL: api/technicians/<id>/  Function: api_show_technicains
- URL: api/appointments/  Function: api_list_appointments
- URL: api/appointments/<id>/  Function: api_show_appointments

## Functions
Api_list_technicains
- GET all technicians data and POST a new technician

Api_show_technicians
- GET a specific technician, PUT allows you to update a specific technician, DELETE a specific technician

Api_list_appointments
- GET all technicians and POST allows you to create a technicians 

Api_show_appointmeents
- GET a specific appointment, PUT allows you to update a specific appointment, DELETE allows you to erase a specific appointment

## Example Json inputs and returns 
### To create a technician
- To POST a request: Use http://localhost:8080/api/technicians/
request body
```
{
	"name": "israel",
	"employee_number": "4"
}
```
Expected return:
```
{
	"id": 1,
	"name": "floyd",
	"employee_number": 1
}
```
### To get List of technicians
- To GET request: Use http://localhost:8080/api/technicians/

Expected return
```
{
	"technicians": [
		{
			"id": 1,
			"name": "floyd",
			"employee_number": 1
		},
		{
			"id": 3,
			"name": "Shelby",
			"employee_number": 3
		},
		{
			"id": 4,
			"name": "israel",
			"employee_number": 4
		},
		{
			"id": 5,
			"name": "ralph",
			"employee_number": 6
		}
	]
}
```
### To get specific technician
To get one tech use: http://localhost:8080/api/technicians/1/
- Expected return 
```
{
	"id": 1,
	"name": "floyd",
	"employee_number": 1
}
```
### To create an appointment
- To POST an appointment use: http://localhost:8080/api/appointments/
- Request body
```
{
	"vin": "1C3CC5FB2AN120174",
	"customer_name" : "Floyd",
	"date" : "2023-1-23 2:30",
	"technician" : 1,
	"reason": "engine light",
	"vip": "True",
	"completed": "True"
}
```
Expected return 
```
{
	"id": 1,
	"vin": "1C3CC5FB2AN120174",
	"customer_name": "Floyd",
	"date": "2023-1-23 2:30",
	"technician": {
		"id": 1,
		"name": "floyd",
		"employee_number": 1
	},
	"reason": "engine light",
	"completed": "True",
	"vip": false
}
```
### To list all appointments 
- To GET all appointments use: http://localhost:8080/api/appointments/
- Expected return
```
{
	"appointments": [
		{
			"id": 2,
			"vin": "1C3CC5FB2AN120174",
			"customer_name": "floyds",
			"date": "2023-01-23T02:30:00+00:00",
			"technician": {
				"id": 1,
				"name": "floyd",
				"employee_number": 1
			},
			"reason": "engine light",
			"completed": true,
			"vip": true
		},
		{
			"id": 3,
			"vin": "12345abcd",
			"customer_name": "Leon ",
			"date": "2023-01-23T00:00:00+00:00",
			"technician": {
				"id": 1,
				"name": "floyd",
				"employee_number": 1
			},
			"reason": "Oil change",
			"completed": false,
			"vip": false
		}
	]
}
```
### To get a specific appointment
- To GET specific appointment use: http://localhost:8080/api/appointments/2/
- Expected return
```
{
	"id": 2,
	"vin": "1C3CC5FB2AN120174",
	"customer_name": "floyd",
	"date": "2023-01-23T02:30:00+00:00",
	"technician": {
		"id": 1,
		"name": "floyd",
		"employee_number": 1
	},
	"reason": "engine light",
	"completed": true,
	"vip": true
}
```


# Routes for API
### HTTP Requests
URLs
- GET List of Manufacturer
http://localhost:8100/api/manufacturers/

- GET Specific Manufacturer
http://localhost:8100/api/manufacturers/:id/

- POST Manufacturer
http://localhost:8100/api/manufacturers/

- PUT Manufacturer
http://localhost:8100/api/manufacturers/:id/

- DELETE Manufacturer
http://localhost:8100/api/manufacturers/:id/

- GET List of Vehicle Model
http://localhost:8100/api/models/

- GET Specific Vehicle Model
http://localhost:8100/api/models/:id/

- POST Vehicle Model
http://localhost:8100/api/models/

- PUT Vehicle Model
http://localhost:8100/api/models/:id/

- DELETE Vehicle Model
http://localhost:8100/api/models/:id/

- GET List of Automobile
http://localhost:8100/api/automobiles/

- GET Specific Automobile
http://localhost:8100/api/automobiles/:vin/

- POST Automobile
http://localhost:8100/api/automobiles/

- PUT Automobile
http://localhost:8100/api/automobiles/:vin/

- DELETE Automobile
http://localhost:8100/api/automobiles/:vin/

- GET List of Sales Person
http://localhost:8090/api/salespersons/

- GET Specific Sales Person
http://localhost:8090/api/salespersons/:id/

- POST Sales Person
http://localhost:8090/api/salespersons/

- PUT Sales Person
http://localhost:8090/api/salespersons/:id/

- DELETE Sales Person
http://localhost:8090/api/salespersons/:id/

- GET List of Sales Customer
http://localhost:8090/api/salescustomers/

- GET Specific Sales Customer
http://localhost:8090/api/salescustomers/:id/

- POST Sales Customer
http://localhost:8090/api/salescustomers/

- PUT Sales Customer
http://localhost:8090/api/salescustomers/:id/

- DELETE Sales Customer
http://localhost:8090/api/salescustomers/:id/

- GET specific technician  http://localhost:8080/api/technicians/:id/

- POST a  technician 		   http://localhost:8080/api/technicians/

- PUT a technician 		  http://localhost:8080/api/technicians/:id/

- GET all technicians		  http://localhost:8080/api/technicians/

- GET specific appointment  http://localhost:8080/api/appointments/:id/

- POST an appointment 		   http://localhost:8080/api/appointments/

- PUT an appointment		  http://localhost:8080/api/appoinmeents/:id/

- GET all appointments		  http://localhost:8080/api/appointments/