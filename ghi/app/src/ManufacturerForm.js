import React, {useEffect, useState} from 'react'

function ManufacturerForm(props) {
    const [manufacturerName, setManufacturerName] = useState("")

    const handleChangeName = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = manufacturerName;

        const manufacturerURL = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturerURL, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer);
            setManufacturerName('');
            props.getManufacturers();
        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Car Manufacturer!</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleChangeName} value={manufacturerName} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="nameChange">Manufacturer Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>



    );

}
export default ManufacturerForm;
