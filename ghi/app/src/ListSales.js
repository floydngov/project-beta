import React from 'react'
import { useEffect, useState } from "react";

function ListSales(props) {
    const [salesRecord, setRecords] = useState([]);
    async function getRecords() {
        const response = await fetch("http://localhost:8090/api/salesrecords/");
        if (response.ok) {
            let data = await response.json();
            setRecords(data.sales_records);
        } else {
            console.log("Message: Bad response");

        }
    }

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: "USD",
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });

    useEffect(() => {getRecords();}, [])
    return (
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Sales Person</th>
            <th>Employee Number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale Price</th>
            </tr>
        </thead>
        <tbody>
            {salesRecord.map(sale => {
            return (
                <tr key={sale.id}>
                    <td>{sale.sales_person.name}</td>
                    <td>{sale.sales_person.emp_no}</td>
                    <td>{sale.sales_customer}</td>
                    <td>{sale.automobile}</td>
                    <td>{formatter.format(sale.price)}</td>
                </tr>
                );
            })}
        </tbody>
    </table>
  )
}

export default ListSales
