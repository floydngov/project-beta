import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers">Manufacturers</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers/new">Add a Manufacturer</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/models">Models</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/models/new">Add Models</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/automobiles">Automobiles</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/automobiles/new">Add Automobiles</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salesperson/new">Salesperson Form</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salescustomers/new/">Customer Form</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/sales">Sales List</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salesperson">Select a sales record</NavLink>
          </li>

          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/sales/new">Create a sales record</NavLink>
          </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="technicians/new/">Create a Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="appointments/new/">Create an Appoinment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="appointments">Appointment List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="appointments/all">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
