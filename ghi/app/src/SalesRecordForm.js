import React, {useState, useEffect} from 'react';

function SalesRecordForm(props) {
    const [autos, setAuto] = useState([])
    const [sales_persons, setSalesPerson] = useState([])
    const [sales_customers, setSalesCustomer] = useState([])
    const [price, setPrice] = useState('')
    const [autoVins, setAutoVins] = useState('')
    const [salesPersonName, setSalesPersonName] = useState('')
    const [salesCustomerName, setSalesCustomerName] = useState('')


    const handleAutoChange = (event) => {
        const value = event.target.value
        setAutoVins(value)
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPersonName(value)
    }

    const handleSalesCustomerChange = (event) => {
        const value = event.target.value
        setSalesCustomerName(value)
    }

    const handlePriceChange =(event) => {
        const value = event.target.value
        setPrice(value)
    }

    useEffect(() => {
        const automobileURL = 'http://localhost:8100/api/automobiles/';
        const salesPersonURL = 'http://localhost:8090/api/salespersons/';
        const salesCustomerURL = 'http://localhost:8090/api/salescustomers/';
        const URLs = [automobileURL, salesPersonURL, salesCustomerURL]
        Promise.all(URLs.map( URL => fetch(URL)
            .then(response => response.json())
        ))
            .then((data) => {
                setAuto(data[0].autos);
                setSalesPerson(data[1].sales_persons);
                setSalesCustomer(data[2].sales_customers);
            })
            .catch(e => console.error('ERROR: ', e))

    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const newSalesRecord = {
            "automobile": autoVins,
            "sales_person": salesPersonName,
            "sales_customer": salesCustomerName,
            "price": price
        }
        const newAutoStatus = {
            "available": false
        }

        const autoUpdateURL = `http://localhost:8100/api/automobiles/${autoVins}/`
        const salesRecordsURL = 'http://localhost:8090/api/salesrecords/';
        const autoConfig = {
            method: "put",
            body: JSON.stringify(newAutoStatus),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const salesConfig = {
            method: "post",
            body: JSON.stringify(newSalesRecord),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        fetch(autoUpdateURL, autoConfig)
            .then(response => response.json())


        fetch(salesRecordsURL, salesConfig)
            .then(response => response.json())
            .then(() => {
                setAutoVins('')
                setSalesPersonName('')
                setSalesCustomerName('')
                setPrice('')
                props.getSales()
            })

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Sales Record</h1>
                    <form onSubmit={handleSubmit} id="create-salesRecord-form">
                        <div className="mb-3">
                            <select onChange={handleAutoChange} required
                            name="autos" id="autos"
                            className="form-select" value={autoVins} >
                                <option value="">Choose an Automobile</option>
                                {autos.map(auto => {
                                    return (
                                        <option key={auto.id} value={auto.vin}>
                                            {auto.year} {auto.model.manufacturer.name} {auto.model.name} VIN: {auto.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalesPersonChange} required
                            name="sales_persons" id="sales_persons"
                            className="form-select" value={salesPersonName} >
                                <option value="">Choose a Sales Person</option>
                                {sales_persons.map(sales_person => {
                                    return (
                                        <option key={sales_person.id} value={sales_person.name}>
                                            {sales_person.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalesCustomerChange} required
                            name="sales_customers" id="sales_customers"
                            className="form-select" value={salesCustomerName} >
                                <option value="">Choose a Sales Customer</option>
                                {sales_customers.map(sales_customer => {
                                    return (
                                        <option key={sales_customer.id} value={sales_customer.name}>
                                            {sales_customer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Price" required
                            type="number" name ="price" id="price"
                            className="form-control" value={price}/>
                            <label htmlFor="price">Price</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div><br/>

            </div>
        </div>
    );

}

export default SalesRecordForm
